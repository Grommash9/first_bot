from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup
from datetime import datetime


def admin_button_inline():
    inline_btn_1 = InlineKeyboardButton('Статистика за сегодня', callback_data='stats_1')
    inline_btn_2 = InlineKeyboardButton('Статистика по дате', callback_data='stats_2')
    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_kb_full.add(inline_btn_1, inline_btn_2)
    return inline_kb_full


def date_inline_calendar(list_date):
    inline_day_kb = InlineKeyboardMarkup(row_width=7)
    date = datetime.now()
    for day in list_date:
        inline_day_kb.insert(
            InlineKeyboardButton(f'{day} / {date.month}', callback_data=f'day_{day}.{date.month}'))
    return inline_day_kb


def generate_workers_button(worker_list):
    inline_day_kb = InlineKeyboardMarkup(row_width=7)
    for worker in worker_list:
        inline_day_kb.insert(InlineKeyboardButton(f'{worker[1]} {worker[2]}', callback_data=f'{worker[0]}'))
    return inline_day_kb


def answer_button(user_id):
    inline_btn_1 = InlineKeyboardButton('Добавить', callback_data=f'add-worker_{user_id}')
    inline_btn_2 = InlineKeyboardButton('Отклонить', callback_data=f'cancel-worker_{user_id}')
    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_kb_full.add(inline_btn_1, inline_btn_2)
    return inline_kb_full


def del_worker(user_id):
    inline_day_kb = InlineKeyboardMarkup(row_width=2)
    inline_day_kb.insert(InlineKeyboardButton(f'Удалить', callback_data=f'del-worker_{user_id}'))
    return inline_day_kb


#
#
# def user_info():
#     inline_btn_1 = InlineKeyboardButton('Список работников', callback_data='work_list')
#     inline_kb_full = InlineKeyboardMarkup(row_width=True)
#     inline_kb_full.add(inline_btn_1)
#     return inline_kb_full
