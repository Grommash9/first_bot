from aiogram import Dispatcher
from aiogram.types import Message
from aiogram.utils import executor
from handlers import admin, user, echo

from config import dp, bot

if __name__ == '__main__':
    executor.start_polling(dp)

