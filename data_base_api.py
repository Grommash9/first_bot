import os
import sqlite3
from datetime import datetime

import check_foto

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
path_to_db = os.path.join(BASE_DIR, 'file_hash.db')


def add_hash(hash):
    with sqlite3.connect(path_to_db) as db:
        db.execute("INSERT INTO hash_data "
                   "(hash) "
                   "VALUES (?)",
                   [hash])
        db.commit()


def add_user(user_id, first_name, second_name):
    with sqlite3.connect(path_to_db) as db:
        db.execute("INSERT INTO user "
                   "(user_id, first_name, second_name) "
                   "VALUES (?, ?, ?)",
                   [user_id, first_name, second_name])
        db.commit()


def set_worker(user_id):
    with sqlite3.connect(path_to_db) as db:
        db.execute("update user set is_worker = 1 where user_id = ?", (user_id,))
        db.commit()


# def get_all_workers():
#     with sqlite3.connect(path_to_db) as db:
#         user = db.execute("SELECT * FROM user where is_worker = 1").fetchall()
#     return user


def add_user_photo(user_id, photo_id, day, month, year):
    with sqlite3.connect(path_to_db) as db:
        db.execute("INSERT INTO users_photo "
                   "(user_id, photo_id, day, month, year) "
                   "VALUES (?, ?, ?, ?, ?)",
                   [user_id, photo_id, day, month, year])
        db.commit()


def get(user_id):
    with sqlite3.connect(path_to_db) as db:
        user = db.execute("SELECT * FROM user where user_id = ?", (user_id,)).fetchone()
    return user


def get_user_name(user_id):
    with sqlite3.connect(path_to_db) as db:
        user = db.execute("SELECT first_name, second_name FROM user where user_id = ?", (user_id,)).fetchone()
    return user


def get_user_photo(user_id, day, month, year):
    with sqlite3.connect(path_to_db) as db:
        users_photo = db.execute("SELECT * FROM users_photo where day = ? and month = ? and year = ? and user_id = ?",
                                 (day, month, year, user_id)).fetchall()
    return users_photo


def get_all_photo(day, month, year):
    with sqlite3.connect(path_to_db) as db:
        users_photo = db.execute("SELECT * FROM users_photo where day = ? and month = ? and year = ?",
                                 (day, month, year,)).fetchall()
    return users_photo


def get_all_users():
    with sqlite3.connect(path_to_db) as db:
        users = db.execute("SELECT user_id, first_name, second_name FROM user where is_worker = 1").fetchall()
    return users


def delete_user(user_id):
    with sqlite3.connect(path_to_db) as db:
        db.execute("DELETE from user where user_id = ?", (user_id,))
        db.commit()


def get_users_for_date(day, month, year):
    with sqlite3.connect(path_to_db) as db:
        users = db.execute("SELECT user.user_id, count(photo_id), user.first_name, user.second_name "
                           " FROM users_photo "
                           "join user "
                           "ON user.user_id = users_photo.user_id "
                           "where day = ? and month = ? and year = ? group by user.user_id",
                           (day, month, year)).fetchall()
    return users


if __name__ == '__main__':
    message_text = 'Статистика за сегодня\n'
    # print(get_users_for_date(3, 8, 2022))
    # for worker in get_users_for_date(2, 8, 2022):
    #     message_text += f"{worker[2]} {worker[3]}: {worker[1]}  - /photo_{worker[0]}\n"
    # print(message_text)
    for worker in get_all_users():
        print(f'{worker[1]} {worker[2]}')



def if_exists(new_hash):
    with sqlite3.connect(path_to_db) as db:
        hash = db.execute("SELECT hash FROM hash_data where hash = ?", (new_hash,)).fetchone()
    return hash
