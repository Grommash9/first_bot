from datetime import datetime
import sys
import time

import aiogram
from aiogram.types import Message, CallbackQuery

import config
import data_base_api
from config import dp, bot, create_time_list
from keyboards import reply, inline
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.contrib.fsm_storage.memory import MemoryStorage

# TODO Handlers reply buttons


class Form(StatesGroup):
    name = State()


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), commands='admin')
async def send_message(message: Message):
    await bot.send_message(message.from_user.id, text='Приветствую Админ',
                           reply_markup=reply.admin_button())


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='Посмотреть статистику')
async def send_message(message: Message):
    await bot.send_message(message.from_user.id, text='Статистика за какой период:',
                           reply_markup=inline.admin_button_inline())


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='Список работников')
async def send_message(message: Message):
    # await bot.send_message(message.from_user.id, text=f'Список работников')
    for worker in data_base_api.get_all_users():
        await bot.send_message(message.from_user.id, text=f'{worker[1]} {worker[2]}     /delete_{worker[0]}')
                               # reply_markup=inline.del_worker(f'{worker[0]}'))



    # await bot.send_message(message.from_user.id, text=f'Список работников',
    #                        reply_markup=inline.generate_workers_button(data_base_api.get_all_users()))

# @dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='Добавить работника')
# async def send_message(message: Message):
#     await bot.send_message(message.from_user.id, text='Введите имя и фамилию работника, которого хотите добавить')
#     await Form.name.set()


# @dp.message_handler(regexp='^[а-яА-Я]{2,} [а-яА-Я]{2,}$', state=Form.name)
# async def add_new_worker(message: Message, state: FSMContext):
#     await state.update_data(name=message.text.lower())
#     async with state.proxy() as data:
#         data['name'] = message.text
#         full_name = data['name'].split(' ')
#         user_first_name = full_name[0]
#         user_second_name = full_name[1]
#     data_base_api.add_user(message.from_user.id, user_first_name, user_second_name)
#     await state.finish()
#     await bot.send_message(message.from_user.id, text='Работник добавлен')
#
#
# @dp.message_handler(state=Form.name)
# async def add_new_worker(message: Message, state: FSMContext):
#     await bot.send_message(message.from_user.id, 'Ошибка ввода. Следуйте примеру')


# @dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='Удалить работника')
# async def send_message(message: Message):
#     await bot.send_message(message.from_user.id, text='Введите user_name рабочего, которого хотите уволить')


# TODO Handlers inline buttons

@dp.callback_query_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='stats_1')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()
    date = datetime.today()
    message_text = 'Статистика за сегодня\n'
    for worker in data_base_api.get_users_for_date(date.day, date.month, date.year):
        message_text += f"{worker[2]} {worker[3]}: {worker[1]}  - /photo_{worker[0]}_{date.day}_{date.month}_{date.year}\n"
    await bot.send_message(call.from_user.id, text=message_text)


@dp.callback_query_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='stats_2')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()
    await bot.send_message(call.from_user.id, text=f'Выберите дату',
                           reply_markup=inline.date_inline_calendar(create_time_list()))


@dp.callback_query_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text_startswith='day_')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()
    today = datetime.today()
    date = call.data.split('_')[-1].split('.')
    day, month = date[0], date[1]
    message_text = f'Статистика за   {day}/{month}:\n'
    for worker in data_base_api.get_users_for_date(day, month, today.year):
        message_text += f"{worker[2]} {worker[3]}: {worker[1]}  - /photo_{worker[0]}_{day}_{month}_{today.year}\n"
    await bot.send_message(call.from_user.id, text=message_text)
    if 'photo' not in message_text:
        await bot.send_message(call.from_user.id, 'Фото за выбраную дату нет')


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text_startswith='/photo')
async def photo_download_command(message: Message):
    try:
        # пробуем разбить команду на параметры, нам нужно что бы нам передали юзер айди и дату
        command_data_list = message.text.split('_')
        target_user_id, day, month, year = command_data_list[1], command_data_list[2],\
                                           command_data_list[3], command_data_list[4]
    except IndexError:
        # если параметров не хватет - кидаем ошибку
        await bot.send_message(message.from_user.id, 'Команда не верна!')
        return
    # получаем список фото пользователя
    user_photo_list = data_base_api.get_user_photo(target_user_id, day, month, year)
    if user_photo_list == 0:
        await bot.send_message(message.from_user.id, 'Фото нет')
    else:
        for data in user_photo_list:
            try:
                await bot.send_photo(message.from_user.id,
                                     photo=data[1],
                                     )
            # обработка неверного file_id, бывает когда меняют токен бота
            except BadRequest:
                pass


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text_startswith='/delete')
async def del_worker(message: Message):
    worker_id = message.text.split('_')[-1]
    data_base_api.delete_user(worker_id)
    # await call.answer('Все ок')
    for admins in config.ADMIN_ID:
        await bot.send_message(admins, text='Работник удален')
        await bot.send_message(worker_id, text='Вас уволили')


@dp.callback_query_handler(text_startswith='add-worker_')
async def new_worker(call: CallbackQuery):
    worker_id = call.data.split('_')[-1]
    data_base_api.set_worker(worker_id)
    await call.answer('Все ок')
    for admins in config.ADMIN_ID:
        await bot.send_message(admins, text='Работник добавлен')
        await bot.send_message(worker_id, text='Ваша заявка одобрена')


@dp.callback_query_handler(text_startswith='cancel-worker_')
async def new_worker(call: CallbackQuery):
    worker_id = call.data.split('_')[-1]
    data_base_api.delete_user(worker_id)
    await call.answer('Все ок')
    for admins in config.ADMIN_ID:
        await bot.send_message(admins, text='Работник отклонен')
        await bot.send_message(worker_id, text='Ваша заявка отклонена')


# if __name__ == '__main__':
