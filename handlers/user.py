import datetime
import os.path

import aiogram
from aiogram.types import Message

import config
import data_base_api
from config import dp, bot
from keyboards import reply
import check_foto


@dp.message_handler(commands='start')
async def send_message(message: Message):
    if message.from_user.id in [i_user[0] for i_user in data_base_api.get_all_users()]:
        await bot.send_message(message.from_user.id,
                           text=f'Привет {message.from_user.username}.'
                                f'\nГрузи фото наклеенных листовок')


@dp.message_handler(content_types=['photo'])
async def send_message(message: Message):
    if message.from_user.id in [i_user[0] for i_user in data_base_api.get_all_users()]:
        file_id = message.photo[-1].file_id
        file_data = await bot.get_file(file_id)
        temp_link = os.path.join(config.temp_photo_folder, '1.jpg')

        await bot.download_file(file_data.file_path, f'{temp_link}')
        result_hash = check_foto.dhash(temp_link)
        if data_base_api.if_exists(result_hash) is None:
            data_base_api.add_hash(result_hash)
            today_date = datetime.datetime.now()
            data_base_api.add_user_photo(message.from_user.id, file_id, today_date.day, today_date.month, today_date.year)
            await bot.send_message(message.from_user.id, text='Ваше фото принято. Спасибо')
        else:
            await bot.send_message(message.from_user.id, text='Ошибка: такое фото уже внесено в базу')
    else:
        await bot.send_message(message.from_user.id, text='Ошибка: Вы не можете грузить фото, поскольку не являетесь'
                                                          ' работником')
