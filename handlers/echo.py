import aiogram
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import Message, CallbackQuery

import config
import data_base_api
from keyboards import inline
from config import dp, bot


class Form(StatesGroup):
    name = State()


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID))
async def send_message(message: Message):
    await bot.send_message(message.from_user.id,
                           text='Ошибочный ввод. Нажмите /admin')


@dp.message_handler(aiogram.filters.IDFilter(chat_id=[i_user[0] for i_user in data_base_api.get_all_users()]))
async def send_message(message: Message):
    if message.from_user.user_id in config.USER_ID:
        await bot.send_message(message.from_user.id,
                           text='Ошибочный ввод. Нажмите /start')


@dp.message_handler()
async def send_message(message: Message, state: FSMContext):
    if message.from_user.id in [i_user[0] for i_user in data_base_api.get_all_users()]:
        await bot.send_message(message.from_user.id, text='Ошибочный ввод. Нажмите /start')
        return
    await bot.send_message(message.from_user.id,
                           text='Вы здесь не работаете. Если хотите зарегистрироваться как работник - отправьте'
                                ' свое имя и фамилию, по примеру Иван Иванов')
    await Form.name.set()


@dp.message_handler(regexp='^[а-яА-Я]{2,} [а-яА-Я]{2,}$', state=Form.name)
async def add_new_worker(message: Message, state: FSMContext):
    await state.update_data(name=message.text.lower())
    async with state.proxy() as data:
        data['name'] = message.text
        full_name = data['name'].split(' ')
        user_first_name = full_name[0]
        user_second_name = full_name[1]
    data_base_api.add_user(message.from_user.id, user_first_name, user_second_name)
    await state.finish()
    await bot.send_message(message.from_user.id, text='заявка отправлена')
    for admins in config.ADMIN_ID:
        await bot.send_message(admins, text=f'{message.text} запросил добавить его в работники',
                               reply_markup=inline.answer_button(message.from_user.id))


@dp.message_handler(state=Form.name)
async def add_new_worker(message: Message, state: FSMContext):
    await bot.send_message(message.from_user.id, 'Ошибка ввода. Следуйте примеру')
